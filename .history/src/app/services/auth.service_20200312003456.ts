import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { DEFAULT_AVATAR, EMAIL_VERIFICATION_ENABLED } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { finalize } from 'rxjs/operators';

import { CommonService } from './common.service';
import { Platform } from '@ionic/angular';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';

import * as firebase from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  verificationId: any;
  user: any;
  celular: string;

  constructor(
    private authFire: FirebaseAuthentication,
    private platform: Platform,
    private afAuth: AngularFireAuth,
    private fireStorage: AngularFireStorage,
    private db: AngularFireDatabase,
    private commonService: CommonService,
  ) { }

  sendOTP(celular): Promise<boolean> {
    return new Promise((resolve, reject) => {  
      this.celular = celular;
      this.platform.ready().then(() => {
        this.authFire.verifyPhoneNumber(this.celular, 30000)
          .then(credential => {
            console.log(credential);
            this.verificationId = credential;
            resolve(true);
        }).catch(err => {
          this.commonService.showAlert('No se pudo enviar el código' + err);
          reject(err);
          console.log(err)
        });
      });
    });
  }

  signIn(otp) {
    return new Promise(async (resolve, reject) => {
      let signInCredential =  await firebase.auth.PhoneAuthProvider.credential(this.verificationId, otp);
      this.afAuth.auth.signInAndRetrieveDataWithCredential(signInCredential)
      .then((success)=>{
        console.log("uid" + success.user.uid);
        resolve(success.user.uid);
      }).catch(er =>{alert(er)});
    });
  }

  setUser(name, photoURL, uid, phoneNumber) {
    let userInfo: any = {
      uid,
      name,
      phoneNumber,
      isPhoneVerified: true,
      photoURL
    };
    // update passenger object
    this.updateUserProfile(userInfo);
  }



  // get current user data from firebase
  getUserData() {
    return this.afAuth.auth.currentUser;
  }

  // get passenger by id
  getUser(id) {
    return this.db.object('passengers/' + id);
  }

  // login by email and password
  login(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  resetPassword(email) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  sendVerificationEmail() {
    return this.afAuth.auth.currentUser.sendEmailVerification();
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  // register new account
  register(email, password, name, phoneNumber) {
    return Observable.create(observer => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password).then((authData: any) => {

        let userInfo: any = {
          uid: authData.user.uid,
          name: name,
          phoneNumber: phoneNumber,
          isPhoneVerified: false,
          email: email
        };

        if (EMAIL_VERIFICATION_ENABLED === true)
          this.getUserData().sendEmailVerification();
        // update passenger object
        this.updateUserProfile(userInfo);
        observer.next();
      }).catch((error: any) => {
        if (error) {
          observer.error(error);
        }
      });
    });
  }

  // update user display name and photo
  async updateUserProfile(user) {
    console.log(user);
    let name = user.name ? user.name : user.email;
    let photoUrl;
    if (user.photoURL) {
      photoUrl = await this.uploadFoto(user.photoURL, user.uid);
    } else {
      photoUrl = DEFAULT_AVATAR;
    }

    this.getUserData().updateProfile({
      displayName: name,
      photoURL: photoUrl
    });

    // create or update passenger
    this.db.object('passengers/' + user.uid).update({
      name: name,
      photoURL: photoUrl,
      phoneNumber: user.phoneNumber ? user.phoneNumber : '',
      isPhoneVerified: user.isPhoneVerified
    })
  }

  // create new user if not exist
  createUserIfNotExist(user) {
    this.getUser(user.uid).valueChanges().pipe(take(1)).subscribe((snapshot: any) => {
      if (snapshot === null) {
        this.updateUserProfile(user);
      }
    });
  }

  // update card setting
  updateCardSetting(number, exp, cvv, token) {
    const user = this.getUserData();
    this.db.object('passengers/' + user.uid + '/card').update({
      number: number,
      exp: exp,
      cvv: cvv,
      token: token
    })
  }

  // get card setting
  getCardSetting() {
    const user = this.getUserData();
    return this.db.object('passengers/' + user.uid + '/card');
  }

  uploadFoto(foto: string, uid): Promise<any> {
    return new Promise (async (resolve, reject) => {
      const ref = this.fireStorage.ref(`passengers/${uid}`);
      const task = ref.putString( foto, 'base64', { contentType: 'image/jpeg'} );

      const p = new Promise ((resolver, rejecte) => {
        const tarea = task.snapshotChanges().pipe(
          finalize(async () => {
            foto = await ref.getDownloadURL().toPromise();
            tarea.unsubscribe();
            resolver(foto);
          })
          ).subscribe(
            x => { },
            err => {
              rejecte(err);
            }
          );
      });
      resolve(p);
    });
  }


}
