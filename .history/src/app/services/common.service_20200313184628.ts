import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController, ActionSheetController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  loader: any;
  constructor(private toastCtrl: ToastController,
    private actionSheetController: ActionSheetController,
    private loadCtrl: LoadingController,
    private alertCtrl: AlertController
  ) { }

  async presentActionSheet() {
    return new Promise(async (resolve, reject) => {      
      const actionSheet = await this.actionSheetController.create({
        header: 'Tomar foto de:',
        buttons: [{
          text: 'Camara',
          icon: 'camera',
          handler: () => {
            resolve('camera');
          }
        }, {
          text: 'Galería',
          icon: 'image',
          handler: () => {
            resolve('galeria');
          }
        }, {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            resolve(null);
          }
        }]
      });
      await actionSheet.present();
    });
  }

  showToast(message) {
    this.toastCtrl.create({ message: message, duration: 3000 }).then(res => res.present());
  }

  showAlert(message) {
    this.alertCtrl.create({
      message: message,
      buttons: ['ok']
    }).then(res => res.present());
  }

  showLoader(message) {
    this.loadCtrl.create({ message: message }).then(res => {
      this.loader = res.present();
      setTimeout(() => this.loadCtrl.dismiss(), 10000);
    });
  }

  hideLoader() {
    this.loadCtrl.dismiss();
  }
}
