import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../services/auth.service';
import { Camera } from '@ionic-native/camera/ngx';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { ENABLE_SIGNUP } from 'src/environments/environment.prod';
import { MenuController, IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild (IonSlides, {static: false}) slide: IonSlides;

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: false,
    loop: false,
    centeredSlides: true,
    speed: 800
  };

  email: string = "";
  password: string = "";
  isRegisterEnabled: any = true;

  nombre = '';
  imagenPreview = '';
  imagen64 = '';
  fotoLista = false;
  celular = '';

  constructor(
    private ngZone: NgZone,
    private camera: Camera,
    private authService: AuthService,
    private translate: TranslateService,
    private commonService: CommonService,
    private router: Router,
    private menuCtrl: MenuController

  ) {
    this.isRegisterEnabled = ENABLE_SIGNUP;
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.slide.lockSwipes(true);
  }

  siguiente() {
    this.slide.lockSwipes(false);
    this.slide.slideNext();
    this.slide.lockSwipes(true);
  }

  async activarCamara() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 600,
      targetHeight: 600,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };
    try {
      const imageData = await this.camera.getPicture(options);
      this.ngZone.run(() => {
        this.imagenPreview = 'data:image/jpeg;base64,' + imageData;
        this.imagen64 = imageData;
        this.fotoLista = true;
      });
    } catch (err) {
      console.log('Error en camara', JSON.stringify(err));
    }
  }

  sendCode() {
    this.celular = this.celular.replace(/ /g, '');
    if (this.celular.length !== 10) {
      this.commonService.showAlert('Número inválido. El número de celular debe ser de 10 dígitos');
      return;
    }
    this.celular = '+52' + this.celular;
    console.log(this.celular);
    this.siguiente();
  }


  reset() {
    if (this.email) {
      this.authService.resetPassword(this.email)
        .then(data => this.commonService.showToast('Please Check inbox'))
        .catch(err => this.commonService.showToast(err.message));
    }
  }

  login() {
    if (this.email.length == 0 || this.password.length == 0) {
      this.commonService.showAlert("Invalid Credentials");
    }
    else {

      this.commonService.showLoader('Authenticating...');
      this.authService.login(this.email, this.password).then(authData => {

        this.commonService.hideLoader();
        this.router.navigateByUrl('/home');
      }, error => {
        this.commonService.hideLoader();
        this.commonService.showToast(error.message);
      });
    }

  }

}
