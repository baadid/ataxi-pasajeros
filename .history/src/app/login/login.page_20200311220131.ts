import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../services/auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { ENABLE_SIGNUP } from 'src/environments/environment.prod';
import { MenuController, IonSlides, IonInput, Platform, ModalController } from '@ionic/angular';
import { CropImagePage } from '../modals/crop-image/crop-image.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild (IonSlides, {static: false}) slide: IonSlides;

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: false,
    loop: false,
    centeredSlides: true,
    speed: 800
  };

  email: string = "";
  password: string = "";
  isRegisterEnabled: any = true;

  nombre = '';
  imagenPreview = '';
  imagen64 = '';
  fotoLista = false;
  celular = '';

  otp = ['', '', '', '', '', ''];
  otpReady = false;

  constructor(
    private ngZone: NgZone,
    private camera: Camera,
    private modalCtrl: ModalController,
    private authService: AuthService,
    private translate: TranslateService,
    private commonService: CommonService,
    private router: Router,
    private platform: Platform,
    private menuCtrl: MenuController

  ) {
    this.isRegisterEnabled = ENABLE_SIGNUP;
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.slide.lockSwipes(true);
  }

  siguiente() {
    this.slide.lockSwipes(false);
    this.slide.slideNext();
    this.slide.lockSwipes(true);
  }

  regresar() {
    this.slide.lockSwipes(false);
    this.slide.slidePrev();
    this.slide.lockSwipes(true);
  }

  async activarCamara() {
    this.platform.ready().then(async () => {
      const options: CameraOptions = {
        cameraDirection: 1,
        correctOrientation: true,
        quality: 50,
        targetWidth: 300,
        targetHeight: 300,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
      };
      try {
        const imageData = await this.camera.getPicture(options);
        this.ngZone.run(() => {
          // this.imagenPreview = 'data:image/jpeg;base64,' + imageData;
          // this.imagen64 = imageData;
          this.cropImage(imageData);
        });
      } catch (err) {
        console.log('Error en camara', JSON.stringify(err));
      }
    });
  }

  async cropImage(imageChangedEvent) {
    const modal = await this.modalCtrl.create({
      component: CropImagePage,
      componentProps: {imageChangedEvent}
    });
    modal.onWillDismiss().then(resp => {
      if (resp.data) {
        this.imagenPreview = resp.data;
        this.imagen64 = resp.data.split('data:image/png;base64,')[1];
        this.fotoLista = true;
      }
    });
    return await modal.present();
  }

  sendCode() {
    this.celular = this.celular.replace(/ /g, '');
    if (this.celular.length !== 10) {
      this.commonService.showAlert('Número inválido. El número de celular debe ser de 10 dígitos');
      return;
    }
    this.celular = '+52' + this.celular;
    console.log(this.celular);
    this.authService.sendOTP(this.celular)
      .then(() => this.siguiente())
      .catch(res => console.log(res));
  }

  moveFocus(nextElement, i) {
    this.otpReady = false;
    setTimeout(() => {      
      if (this.otp[i].length === 0) {
        return;
      }
      if (this.otp[i+1]) {
        this.otp[i+1] = '';
      }
      nextElement.setFocus();
    }, 100);
  }

  enableOtpButton() {
    let ready = true;
    setTimeout(() => {      
      for (let i = 0; i < this.otp.length; i++) {
        if (!this.otp[i]) {
          ready = false;
          break;
        }
      }
      if (ready) {
        this.otpReady = true
      } else {
        this.otpReady = false;
      }
    }, 100);
  }

  validarOTP() {
    const otp = this.otp.join().replace(/,/g, '');
    alert(otp);
    this.authService.signIn(otp)
      .then((uid) => {
        this.authService.setUser(this.nombre, this.imagen64, uid, this.celular);
        this.router.navigateByUrl('/home')
      });
  }

  reset() {
    if (this.email) {
      this.authService.resetPassword(this.email)
        .then(data => this.commonService.showToast('Please Check inbox'))
        .catch(err => this.commonService.showToast(err.message));
    }
  }

  login() {
    if (this.email.length == 0 || this.password.length == 0) {
      this.commonService.showAlert("Invalid Credentials");
    }
    else {

      this.commonService.showLoader('Authenticating...');
      this.authService.login(this.email, this.password).then(authData => {

        this.commonService.hideLoader();
        this.router.navigateByUrl('/home');
      }, error => {
        this.commonService.hideLoader();
        this.commonService.showToast(error.message);
      });
    }

  }

}
